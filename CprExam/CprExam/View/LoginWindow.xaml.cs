﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CprExam.Model;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;

namespace CprExam.View
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        public LoginWindow()
        {
            InitializeComponent();

            Messenger.Default.Register<object>(this, MessageToken.LoginSuccess, (i) =>
            {
                ExamineeLogin win = new ExamineeLogin();
                win.Show();
                Close();
            });


            Messenger.Default.Register<string>(this, MessageToken.LoginFail, (i) =>
            {
                MessageBox.Show("登录失败：" + i);
            });
        }
    }
}
