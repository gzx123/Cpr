﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CprExam.Model;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;

namespace CprExam.View
{
    /// <summary>
    /// ExamingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ExamingWindow : MetroWindow
    {
        public ExamingWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<object>(this, MessageToken.BackLogin,
              (o) =>
              {

                  ExamineeLogin.Show();
                  this.Hide();
              });
        }

        public ExamineeLogin ExamineeLogin { get; set; }
    }
}
