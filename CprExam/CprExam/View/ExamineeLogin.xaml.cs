﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CprExam.Model;
using GalaSoft.MvvmLight.Messaging;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace CprExam.View
{
    /// <summary>
    /// ExamineeLogin.xaml 的交互逻辑
    /// </summary>
    public partial class ExamineeLogin : MetroWindow
    {
        private ExamingWindow _window;
        ProgressDialogController controller;
        public ExamineeLogin()
        {
            this.Closing += ExamineeLogin_Closing;
            InitializeComponent();
            _window  = new ExamingWindow() { ExamineeLogin = this};
            Messenger.Default.Register<string>(this, MessageToken.ShowLoginMessage, async (i) =>
            {
                await this.ShowMessageAsync(i, "");

            });

            Messenger.Default.Register<string>(this, MessageToken.ShowProgressMessage, async (i) =>
            {
                controller = await this.ShowProgressAsync(i, "");
                controller.SetIndeterminate();
            });

            Messenger.Default.Register<string>(this, MessageToken.CloseProgressMessage, async (i) =>
            {
                if (controller != null)
                    await controller.CloseAsync();
            });

            Messenger.Default.Register<ExamMessage>(this, MessageToken.ShowExamingWindow,
              (o) =>
              {
                  _window.Show();
                  Hide();
              });

        }

        private void ExamineeLogin_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           Application.Current.Shutdown();
        }
    }
}
