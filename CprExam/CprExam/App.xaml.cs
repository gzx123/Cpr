﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CprExam.View;
using GLCommon;

namespace CprExam
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            LogTool.Init();
            InitHttpTool();
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            if (Global.SystemModel == SystemModel.online)
            {
                LoginWindow win = new LoginWindow();
                win.Show();
            }
            else
            {
                ExamineeLogin win = new ExamineeLogin();
                win.Show();
            }
        }

        private void InitHttpTool()
        {
            var configs = ConfigTool.GetConfig();
            var serverAddress = configs.SingleOrDefault(i => i.name == "ServerAddress");
            if (serverAddress != null)
            {
                var address = serverAddress.value;
                HttpTool.Client.BaseAddress = new Uri(address);
            }
        }

        private async void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Console.WriteLine(e.Exception);
            e.Handled = true;

            LogTool.UpServerLog(e.Exception.StackTrace, EventType.Error);

        }

        private async void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("捕获异常" + DateTime.Now.ToShortTimeString());
            Exception exception = e.ExceptionObject as Exception;
            Console.WriteLine(exception.StackTrace);
            LogTool.UpServerLog(exception.StackTrace, EventType.Error);

        }
    }
}
