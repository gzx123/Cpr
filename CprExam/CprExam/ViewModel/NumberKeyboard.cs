﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CprExam.Model;
using CprExam.ViewModel;

namespace CprExam.ViewModel
{
    using System.Windows;

    using GalaSoft.MvvmLight.Command;
    
    using GalaSoft.MvvmLight.Messaging;

    public class NumberKeyboardViewModel : VmBase
    {
        #region 命令
        /// <summary>
        /// 点击数字命令
        /// </summary>
        public RelayCommand<string> ClickNumberCommand { get;  set; }
        /// <summary>
        /// 删除数字命令
        /// </summary>
        public RelayCommand DeleteNumberCommand { get;  set; }
        /// <summary>
        /// 确定字母
        /// </summary>
        public RelayCommand SureNumberCommand { get;  set; }
        #endregion

        #region 参数
        private String _numberText;
        public String NumberText
        {
            get { return _numberText; }
            set { _numberText = value; RaisePropertyChanged("NumberText"); }
        }



        #endregion

        public NumberKeyboardViewModel()
        {
            ClickNumberCommand = new RelayCommand<string>(AppendText);
            DeleteNumberCommand = new RelayCommand(DeleteText);
            SureNumberCommand = new RelayCommand(SendNumberForMessage);

            Register(this, MessageToken.ClearNumberText,
                s =>
                {
                    NumberText = "";
                });
            Register<string>(this, MessageToken.SendNumberText,
                s =>
                {
                    NumberText = s;
                });
        }

        /// <summary>
        /// 发送数字到消息
        /// </summary>
        private void SendNumberForMessage()
        {
            Send<string>(NumberText, MessageToken.SendNumberText);
        }

        private void DeleteText()
        {
            if (!String.IsNullOrEmpty(NumberText) && NumberText.Length > 0)
            {
                NumberText = NumberText.Length == 1 ? "" : NumberText.Substring(0, NumberText.Length - 1);
            }
        }

        private void AppendText(string text)
        {
            NumberText += text;
            Send<string>(NumberText, MessageToken.SendNumberText);
        }

    }
}
