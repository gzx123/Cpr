﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CprExam.Model
{
    /// <summary>
    /// 消息token
    /// </summary>
    public enum MessageToken
    {
        ShowExamingWindow,
        ClearNumberText,
        SendNumberText,
        ExameeLoginMessage,
        ShowInputWindow,
        LoginSuccess,
        LoginFail,
        ShowProgressMessage,
        ShowLoginMessage,
        CloseProgressMessage,
        BackLogin
    }
}
