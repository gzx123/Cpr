﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CprExam.Model
{
    public class ExamineeInfoApi
    {
        public string code { get; set; }
        public int status { get; set; }
        public int id { get; set; }
        public string idcard { get; set; }
        public string nr { get; set; }
        public string name { get; set; }
        public string photo { get; set; }
        public string finger { get; set; }
        public string wtroot { get; set; }
        public string wtadded { get; set; }

        public byte[] photoByte { get; set; }
    }

    /// <summary>
    /// 考试消息
    /// </summary>
    public class ExamMessage
    {
        public ExamineeInfoApi ExamineeInfo { get; set; }
        public SerialPort SerialPort { get; set; }
    }
}
