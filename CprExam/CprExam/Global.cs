﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CprExam
{
   public static class Global
    {
        public static SystemModel SystemModel;

        static Global()
        {
            SystemModel = GetModel();


        }

       public static string ClientName { get; set; }

       /// <summary>
        /// 初始化软件模式（在线、离线）
        /// </summary>
        private static SystemModel GetModel()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = config.AppSettings.Settings;
            var item = settings["Model"];
            if (item == null)
            {
                throw new Exception("Model节点未配置");
            }
            try
            {
                var model = (SystemModel)Enum.Parse(typeof(SystemModel), item.Value.ToLower());
                return model;
            }
            catch (Exception)
            {
                 throw  new Exception("Model节点配置错误，只能配置Online,Offline");
            }
           
        }
    }

   public  enum SystemModel
    {
      online,offline
    }
}
